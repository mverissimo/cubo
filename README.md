# CUBO

##Libaries used
  - react
  - react-router
  - react-toastify

  - redux
  - redux sagas
  - chart.js
  - formik
  - axios

  - mongo
  - helmet
  - validator

# Commands

Tests
```sh
$ npm run test:server
```

Development
```sh
$ npm run dev:server
$ npm run dev:client
or
$ npm run dev
```

Run
```sh
$ npm install
or
$ yarn install
```

# Observation
I set it up only configure env for dev
