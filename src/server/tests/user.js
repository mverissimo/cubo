const chai = require('chai');
const chaiHttp = require('chai-http');

const User = require('../../models/user');

chai.should();
chai.use(chaiHttp);

const request = chai.request;

describe('User', () => {
  describe('/GET users', () => {
    it('it should GET all users', done => {
      request('http://localhost:3001/')
        .get('api/user/data')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          res.body.status.should.be.a('string');
          res.body.data.should.be.a('array');

          done();
        });
    });
  });

  describe('/POST user', () => {
    it('it should POST a user', done => {
      let user = {
        "name": 'user',
        "surname": 'surname',
        "participation": '10'
      };

      request('http://localhost:3001/')
        .post('api/user')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name');
          res.body.should.have.property('surname');
          res.body.should.have.property('participation');

          done();
        });
    });
  });
});
