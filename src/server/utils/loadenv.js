const dotenv = require("dotenv");
const path = require("path");

// workaround based on https://github.com/motdotla/dotenv/issues/133
let envPath = path.resolve(__dirname, "..", "..", "app", ".env");

dotenv.config({ "path": envPath });
