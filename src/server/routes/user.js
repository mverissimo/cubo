const User = require('../controllers/user');
const wrapAsync = require('../utils/controller').wrapAsync;

module.exports = app => {
  app.route('/api/user').post(wrapAsync(User.add));
  app.route('/api/user/data').get(wrapAsync(User.data));
};
