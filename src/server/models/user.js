const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamps');

const config = require('../config');

const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    "name": {
      "type": String,
      "trim": true,
      "required": true
    },
    "surname": {
      "type": String,
      "trim": true,
      "required": true
    },
    "participation": {
      "type": Number,
      "trim": true,
      "required": true
    }
  },
  {
    "collection": 'users'
  }
);

UserSchema.plugin(timestamps, {
  "createdAt": { "index": true },
  "updatedAt": { "index": true }
});

UserSchema.index({ "name": 1 });

UserSchema.methods.serializedUser = function serializedUser() {
  const user = this;

  const serialized = {
    "_id": user._id,
    "name": user.name,
    "surname": user.surname,
    "participation": user.participation
  };

  return serialized;
};

module.exports = exports = mongoose.model('User', UserSchema);
