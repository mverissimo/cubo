require('@babel/polyfill');

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const compression = require('compression');
const limit = require('express-rate-limit');

const config = require('./config');
const logger = require('./utils/logger');
const mongo = require('./config/db');
const routes = require('./routes');

const app = express();

app.use(cors());
app.use(compression());
app.use(helmet());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ "extended": true }));
app.use(morgan('dev'));

app.enable('trust proxy');
app.use(
  new limit({
    "windowMs": 60 * 1000,
    "max": 100,
    "delayMs": 0
  })
);

// Serve /public static files when unauth
app.use('/public', express.static(path.join(__dirname, '../../public')));

// Serve /dist static diles when auth
app.use('/dist', express.static(path.join(__dirname, '../../dist')));

// Init routes
routes(app);

// Init mongodb
mongo();

// Init express server
app.listen(config.server.port, err => {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  logger.info(
    `API is now running on port ${config.server.port} in ${config.env} mode`
  );
});
