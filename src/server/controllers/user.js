const validator = require('validator');
const User = require('../models/user');

exports.add = async (req, res) => {
  const data = req.body || {};

  // Check empty fields
  if (!data.name || !data.surname || !data.participation) {
    return res.status(400).json({ "message": 'Missing required fields.' });
  }

  // Remove white spaces
  data.name = data.name.trim();
  data.surname = data.surname.trim();

  if (!validator.isNumeric(data.participation)) {
    return res
      .status(400)
      .json({ "message": 'Invalid participation, just number.' });
  }

  const user = await User.create({
    "name": data.name,
    "surname": data.surname,
    "participation": data.participation
  });

  res.json(user.serializedUser());
};

exports.data = async (req, res) => {
  const users = await User.find();

  res.status(200).json({
    "status": 'success',
    "data": users
  });
};
