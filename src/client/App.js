import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';

import './styles/index.css';
import 'react-toastify/dist/ReactToastify.css';

import Router from './routes';

class App extends Component {
  render() {
    const { store, history } = this.props;

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Router />
        </ConnectedRouter>
      </Provider>
    );
  }
}

App.propTypes = {
  "store": PropTypes.object.isRequired,
  "history": PropTypes.object.isRequired
};

export default App;
