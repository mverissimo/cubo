import axios from 'axios';

const fetch = (method, path, data, params, contentType) => {
  if (!method) {
    throw new Error('Method is a required field.');
  }

  if (!path) {
    throw new Error('Path is a required field.');
  }

  const BASE_URL = 'http://0.0.0.0:3001';

  let options = {
    "baseURL": BASE_URL,
    "data": data || {},
    "headers": {
      'Content-Type': 'application/json'
    },
    "method": method.toUpperCase(),
    "params": params || {},
    "url": path
  };

  return axios(options);
};

export default fetch;
