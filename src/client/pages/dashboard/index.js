import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Container, Row, Col } from 'react-grid-system';
import { ToastContainer } from 'react-toastify';

import Header from './components/header';
import FormUser from './components/form';
import Table from './components/table';
import ChartUser from './components/chart';

import {
  Actions as userActions,
  Selectors as userSelectors
} from '../../redux/modules/user';

class DashboardPage extends Component {
  componentDidMount() {
    const { actions } = this.props;

    actions.user.requestListUsers();
  }

  static propTypes = {
    "data": PropTypes.object,
    "dataset": PropTypes.object,
    "actions": PropTypes.object
  };

  handleSubmit = values => {
    const { actions } = this.props;

    actions.user.requestAddUser(values);
  };

  render() {
    const tableData = {
      "columns": ['', 'First Name', 'Last Name', 'Participation'],
      "rows": this.props.data.users
    };

    return (
      <Fragment>
        <Header>
          <FormUser onSubmit={this.handleSubmit} />
        </Header>

        <Container>
          <Row>
            <Col>
              <h1 className="dashboard-title">Data</h1>
              <p className="dashboard-text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              </p>
            </Col>
          </Row>

          <Row>
            <Col sm={12} md={6}>
              <Table data={tableData} />
            </Col>

            <Col sm={12} md={6}>
              {this.props.data.users.length <= 0 ? (
                <span className="dashboard-text">No user data</span>
              ) : (
                <ChartUser data={this.props.data.dataset} />
              )}
            </Col>
          </Row>
        </Container>

        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    "data": {
      "users": state.user.list,
      "dataset": userSelectors.generateDataset(state.user.list)
    }
  };
};

const mapDispatchToProps = dispatch => ({
  "actions": {
    "user": bindActionCreators(userActions, dispatch)
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardPage);
