import React from 'react';
import PropTypes from 'prop-types';

const Header = ({ children }) => <header className="dashboard-header">{children}</header>;

Header.propTypes = {
  "children": PropTypes.node
};

export default Header;
