import React from 'react';
import PropTypes from 'prop-types';

import { Doughnut } from 'react-chartjs-2';

const ChartUser = ({ data }) => {
  const legendOpts = {
    "position": 'right',
    "labels": {
      "padding": 20,
      "fontSize": 14,
      "fontStyle": 'bold'
    }
  };

  return (
    <Doughnut
      data={{
        "labels": data.labels,
        "datasets": [
          {
            "data": data.dataset,
            "backgroundColor": data.colors
          }
        ]
      }}
      legend={legendOpts}
    />
  );
};

ChartUser.propTypes = {
  "data": PropTypes.object
};

export default ChartUser;
