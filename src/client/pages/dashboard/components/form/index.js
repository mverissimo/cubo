import React from 'react';
import PropTypes from 'prop-types';

import { withFormik, Field } from 'formik';
// import { Field, reduxForm } from 'redux-form';
import validator from 'validator';

import { Container, Row, Col } from 'react-grid-system';

import Input from '../../../../components/Input';

const FormUser = ({
  values,
  touched,
  errors,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting
}) => (
  <form onSubmit={handleSubmit}>
    <Container>
      <Row>
        <Col sm={12} md={3}>
          <Field
            name="name"
            placeholder="First Name"
            component={Input}
            type="text"
          />
        </Col>
        <Col sm={12} md={3}>
          <Field
            name="surname"
            placeholder="Last Name"
            component={Input}
            type="text"
          />
        </Col>
        <Col sm={12} md={3}>
          <Field
            name="participation"
            placeholder="Participation"
            component={Input}
            type="text"
          />
        </Col>
        <Col sm={12} md={3}>
          <button type="submit" className="btn" disabled={isSubmitting}>
            send
          </button>
        </Col>
      </Row>
    </Container>
  </form>
);

FormUser.propTypes = {
  "values": PropTypes.object,
  "touched": PropTypes.object,
  "errors": PropTypes.object,
  "isSubmitting": PropTypes.bool,
  "handleChange": PropTypes.func,
  "handleBlur": PropTypes.func,
  "handleSubmit": PropTypes.func
};

const UserEnhancedForm = withFormik({
  "mapPropsToValues": () => ({ "name": '', "surname": '', "participation": '' }),
  "validate": values => {
    const errors = {};

    if (!values.name) {
      errors.name = 'Required';
    }

    if (!values.surname) {
      errors.surname = 'Required';
    }

    if (!values.participation) {
      errors.participation = 'Required';
    } else if (!validator.isNumeric(values.participation)) {
      errors.participation = 'Only numbers';
    }

    return errors;
  },
  "handleSubmit": (values, { props, setSubmitting, resetForm }) => {
    props.onSubmit(values);
    resetForm();
    setSubmitting(false);
  },
  "displayName": 'FormUser'
})(FormUser);

export default UserEnhancedForm;

// export default reduxForm({
//   "form": 'FormUser',
//   validate
// })(FormUser);
