import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

const Head = ({ columns }) => (
  <thead>
    <tr>
      {columns.map((column, index) => (
        <th scope="col" key={index}>
          {column}
        </th>
      ))}
    </tr>
  </thead>
);

const Body = ({ rows }) => (
  <tbody>
    {rows.length <= 0 ? (
      <tr>
        <td className="text-center" colSpan="4">
          no user data
        </td>
      </tr>
    ) : (
      rows.map((user, index) => (
        <tr key={index}>
          <td className="text-center">{index}</td>
          <td>{user.name}</td>
          <td>{user.surname}</td>
          <td>{`${user.participation}%`}</td>
        </tr>
      ))
    )}
  </tbody>
);

const Table = ({ data }) => {
  return (
    <table>
      <Head columns={data.columns} />
      <Body rows={data.rows} />
    </table>
  );
};

Head.propTypes = {
  "columns": PropTypes.array
};

Body.propTypes = {
  "rows": PropTypes.array
};

Table.propTypes = {
  "data": PropTypes.object
};

export default Table;
