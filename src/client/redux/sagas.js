import { fork } from 'redux-saga/effects';
import UserSagas from './modules/user/sagas';

export default function* root() {
  yield fork(UserSagas);
}
