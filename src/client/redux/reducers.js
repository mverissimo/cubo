import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { Reducer as UserReducer } from './modules/user/';

export default combineReducers({
  "routing": routerReducer,
  "user": UserReducer
});
