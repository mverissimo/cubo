import randomColor from 'randomcolor';

export const generateDataset = state => {
  if (state.length) {
    const participation = [];
    const fullname = [];
    const colors = [];

    state.map(user => {
      participation.push(user.participation);
      fullname.push(`${user.name} ${user.surname}`);
      colors.push(randomColor());
    });

    const data = {
      "labels": fullname,
      "dataset": participation,
      "colors": colors
    };

    return data;
  }
};
