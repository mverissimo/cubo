import {
  take,
  takeLatest,
  takeEvery,
  call,
  put,
  select,
  fork
} from 'redux-saga/effects';

import { toast } from 'react-toastify';

import * as api from '../api';

import {
  USER_ADD_REQUEST,
  USER_ADD_FAILURE,
  USER_ADD_SUCCESS,
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
  USER_LIST_FAILURE
} from './constants';

import {
  requestAddUser,
  requestAddUserSuccess,
  requestAddUserFailure,
  requestListUsers,
  requestListUsersSuccess,
  requestListUsersFailure
} from './actions';

function* newUser() {
  while (true) {
    try {
      const payload = yield take(USER_ADD_REQUEST);

      const user = yield call(api.add, payload.data);

      if (user.status === 200) {
        yield put(requestAddUserSuccess(user));

        yield put(requestListUsers());
      }
    } catch (e) {
      yield call(toast.error, e.data.message);
      yield put(requestAddUserFailure(e.data.message));
    }
  }
}

function* list() {
  try {
    const users = yield call(api.list);

    yield put(requestListUsersSuccess(users));
  } catch (e) {
    yield call(toast.error(e.data.message));
    yield put(requestListUsersFailure(e.data.message));
  }
}

export default function* root() {
  yield fork(newUser);
  yield takeEvery(USER_LIST_REQUEST, list);
}
