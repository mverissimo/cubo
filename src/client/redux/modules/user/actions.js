import {
  USER_ADD_REQUEST,
  USER_ADD_FAILURE,
  USER_ADD_SUCCESS,
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
  USER_LIST_FAILURE
} from './constants';

/**
 * Add
 */
export const requestAddUser = data => ({
  "type": USER_ADD_REQUEST,
  data
});

export const requestAddUserSuccess = () => ({
  "type": USER_ADD_SUCCESS
});

export const requestAddUserFailure = error => ({
  "type": USER_ADD_FAILURE,
  error
});

/**
 * List
 */
export const requestListUsers = () => ({
  "type": USER_LIST_REQUEST
});

export const requestListUsersSuccess = users => ({
  "type": USER_LIST_SUCCESS,
  users
});

export const requestListUsersFailure = error => ({
  "type": USER_LIST_FAILURE,
  error
});
