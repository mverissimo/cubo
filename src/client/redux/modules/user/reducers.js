import {
  USER_ADD_REQUEST,
  USER_ADD_FAILURE,
  USER_ADD_SUCCESS,
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
  USER_LIST_FAILURE
} from './constants';

const initialState = {
  "list": [],
  "error": '',
  "isRequest": false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case USER_ADD_REQUEST:
      return {
        ...state,
        "isRequest": true
      };

      break;

    case USER_ADD_SUCCESS:
      return {
        ...state,
        "isRequest": false
      };

      break;

    case USER_ADD_FAILURE:
      return {
        ...state,
        "error": action.error,
        "isRequest": false
      };

      break;

    case USER_LIST_REQUEST:
      return {
        ...state,
        "isRequest": true
      };

      break;

    case USER_LIST_SUCCESS:
      return {
        ...state,
        "isRequest": false,
        "list": action.users.data
      };

      break;

    case USER_LIST_FAILURE:
      return {
        ...state,
        "isRequest": false,
        "error": action.error
      };

      break;

    default:
      return state;
  }
}
