import fetch from '../../utils/fetch';

const API_USER_ENDPOINT = 'http://localhost:3001/api/user';

/**
 * User
 */
export async function add(data) {
  const response = fetch('POST', API_USER_ENDPOINT, data)
    .then(response => response)
    .catch(err => {
      throw err.response;
    });

  return response;
}

export async function list() {
  const response = fetch('GET', `${API_USER_ENDPOINT}/data`)
    .then(response => response.data)
    .catch(err => {
      throw err.response;
    });

  return response;
}
