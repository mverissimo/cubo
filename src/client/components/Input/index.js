import React from 'react';
import cn from 'classnames';

const Input = ({ field, "form": { touched, errors }, ...props }) => {
  const inputClass = cn({
    "input": true,
    'input--danger': touched[field.name] && errors[field.name]
  });

  return (
    <div className="form-group">
      <input {...field} {...props} className={inputClass} />
    </div>
  );
};

export default Input;
