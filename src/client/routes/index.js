import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { hot } from 'react-hot-loader';

import DashboardPage from '../pages/dashboard';

class Router extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={DashboardPage} />
      </Switch>
    );
  }
}

export default hot(module)(Router);
